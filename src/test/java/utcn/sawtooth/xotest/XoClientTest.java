package utcn.sawtooth.xotest;

import co.nstant.in.cbor.CborBuilder;
import co.nstant.in.cbor.CborEncoder;
import co.nstant.in.cbor.CborException;

import com.google.protobuf.ByteString;

import org.bitcoin.NativeSecp256k1Util;
import org.bitcoinj.core.ECKey;

import org.junit.Test;
import sawtooth.sdk.client.Signing;
import sawtooth.sdk.protobuf.*;


import java.io.*;
import java.net.*;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import static org.junit.Assert.*;

/**
 * Testing java client integration with sawtooth rest api server.
 *
 * @author evo
 */
public class XoClientTest {


    @Test
    public void testSignature(){
        try {
            ECKey privateKey = Signing.generatePrivateKey(SecureRandom.getInstance("SHA1PRNG"));
            String publicKey = Signing.getPublicKey(privateKey);

            System.out.println("Generated pair: \n\n"+privateKey+"\n-----------\n"+publicKey);

            assertNotNull(publicKey);

        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }

    }

    /**
     * Submit a transaction.
     *
     * Use secp256k1 eliptic curve and generate a 64-byte signature from the hash of the headers.
     * @throws CborException
     * @throws NoSuchAlgorithmException
     * @throws NativeSecp256k1Util.AssertFailException
     */
    @Test
    public void submitTransaction() throws CborException, NoSuchAlgorithmException, NativeSecp256k1Util.AssertFailException {

        // Create private key
        ECKey privateKey = Signing.generatePrivateKey(SecureRandom.getInstance("SHA1PRNG"));

        // Generate public key from the private key
        String publicKey = Signing.getPublicKey(privateKey);

        // Encode payload. Payload is transparent to validator and is application level speciffic.
        ByteArrayOutputStream boas = new ByteArrayOutputStream();
        new CborEncoder(boas).encode(new CborBuilder()
                .addMap()
                .put("Verb","set")
                .put("Name","foo_test_utcn")
                .put("Value",1977)
                .end()
                .build());

        byte[] transactionPayload = boas.toByteArray();

        // Create has 512 of the payload
        String payloadHashSHA512 = sawtooth.sdk.processor.Utils.hash512(transactionPayload);

        TransactionHeader header = TransactionHeader.newBuilder()
                .setFamilyName("intkey")
                .setFamilyVersion("1.0")
                //.addInputs("1cf126487ef279228b7215f7f845f07f76e6bd966c7490b0db59d81a8d803c5e3a674d")
                //.addOutputs("1cf126487ef279228b7215f7f845f07f76e6bd966c7490b0db59d81a8d803c5e3a674d")
                .setPayloadSha512(payloadHashSHA512)
                .setSignerPublicKey(publicKey)
                .setBatcherPublicKey(publicKey)
                .build();

        String headerString = header.toString();
        System.out.println("Header:\n"+headerString);
        assertNotNull(headerString);


        //create bitcoin like signature
        //ECKey.ECDSASignature headerSignature = privateKey.sign(Sha256Hash.of(header.toByteArray()));
        String headerSignature = Signing.sign(privateKey, header.toByteArray());
        System.out.println("Transaction header signature:"+headerSignature);
        assertEquals(128, headerSignature.length());

        //create transaction
        Transaction transaction = Transaction.newBuilder()
                .setHeader(header.toByteString())
                .setHeaderSignature(headerSignature)
                .setPayload(ByteString.copyFrom(transactionPayload))
                .build();


        System.out.println("\n\nTransaction:\n"+transaction.toString());

        //create batch header
        BatchHeader batcherHeader = BatchHeader.newBuilder()
                .setSignerPublicKey(publicKey) //todo check this
                .addTransactionIds(transaction.getHeaderSignature())
                .build();

        String batchHeaderSignature = Signing.sign(privateKey, batcherHeader.toByteArray());
        System.out.println("Batch header signature:"+batchHeaderSignature);
        assertEquals(128, batchHeaderSignature.length());

        //create batch
        Batch batch = Batch.newBuilder()
                .setHeader(batcherHeader.toByteString())
                .setHeaderSignature(batchHeaderSignature)
                .addTransactions(transaction)
                .build();

        //create batch list
        BatchList batchList = BatchList.newBuilder()
                .addBatches(batch)
                .build();

        //submit batch to validator
        System.out.println("Posting data!");
        String response = sendPostRequest("http://localhost:8080/batches", batchList.toByteArray());
        assertTrue(response.indexOf("link")!=-1);

    }

    private String sendPostRequest(String urlString, byte[] data) {
        try {

            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/octet-stream");

            OutputStream os = conn.getOutputStream();
            os.write(data);
            os.flush();

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            System.out.println("Output from Server .... \n");
            StringBuffer sb = new StringBuffer();
            while ((output = br.readLine()) != null) {
                System.out.println(output);
                sb.append(output).append("\n");
            }

            conn.disconnect();
            return sb.toString();

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }

        return "";
    }
}
